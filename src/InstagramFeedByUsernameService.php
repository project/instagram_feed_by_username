<?php

namespace Drupal\instagram_feed_by_username;

use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\ClientInterface;
use Drupal\Component\Render\FormattableMarkup;
use GuzzleHttp\Exception\GuzzleException;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\RendererInterface;

/**
 * The InstaService service class.
 */
class InstagramFeedByUsernameService {
  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;
  /**
   * Constructor for InstaService.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   A Guzzle client object.
   */
  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public function __construct(ClientInterface $http_client, MessengerInterface $messenger, RendererInterface $renderer) {
    $this->httpClient = $http_client;
    $this->messenger = $messenger;
    $this->renderer = $renderer;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get("http_client"),
          $container->get('renderer'),
          );

  }

  /**
   * Posts route callback.
   *
   * @param string $userName
   *   For passing the username.
   *
   * @return htmldata
   *   return html data
   */
  public function posts($userName) {
    $userInstaFeeds = "";
    try {
      /*
       * We use Guzzle to make an HTTP request.
       */
      $request = $this->httpClient->request(
            "GET",
            "https://api.woxo.tech/instagram?source=" . $userName,
            []
        );
    }
    catch (GuzzleException $error) {
      /*
       * Here we catch the instance of GuzzleHttp\Psr7\Response
       * So you can have: HTTP status code, message, headers and body.
       * Just check the exception object has the response before.
       */
      $request = $error->getResponse();
      $posts = $request->getBody()->getContents();
      $message = new FormattableMarkup(
            "API connection error. Error details are as follows:<pre>@response</pre>",
            ["@response" => print_r(json_decode($posts), TRUE)]
            );
      watchdog_exception("Remote API Connection", $error, $message);
    }
    // If the response is success.
    if ($request->getStatusCode() != 200) {
      return $userInstaFeeds;
    }
    $posts = $request->getBody()->getContents();
    $data = Json::decode($posts);
    if (!empty($data)) {
      // Render output using instagram_feed_by_username theme.
      $renderable = [
        "#theme" => "field__instagram_feed_by_username",
        "#data" => $data,
      ];
      $userInstaFeeds = $this->renderer->renderPlain(
            $renderable
        );
    }
    else {
      $this->messenger->addWarning(
            $this->t("Instagram username is not correct")
            );
      $userInstaFeeds = "Cannot find the username";
    }
    $message = new FormattableMarkup(
          "API connection error. Error details are as follows:<pre>@response</pre>",
          ["@response" => print_r(json_decode($posts), TRUE)]
      );
    return $userInstaFeeds;
  }

}
