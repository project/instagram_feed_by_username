<?php

namespace Drupal\instagram_feed_by_username\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\instagram_feed_by_username\InstagramFeedByUsernameService;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Plugin implementation of the'InstagramFieldByUsernameFieldFormatter'.
 *
 * @FieldFormatter
 * (
 *   id = "InstagramFieldByUsernameFieldFormatter",
 *   label = @Translation("Instagram username field formatter"),
 *   field_types = {
 *     "instagram_field"
 *   }
 *  )
 */
class InstagramFeedByUsernameFieldFormatter extends FormatterBase implements ContainerFactoryPluginInterface {
  /**
   * Define how the field type is showed.
   *
   * Inside this method we can customize how the field is displayed inside
   * pages.
   */
  /**
   * {@inheritdoc}
   */
  /**
   * The field definition.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface
   */
  protected $fieldDefinition;
  /**
   * The example service.
   *
   * @var Drupal\instagram_feed_by_username\InstagramFeedByUsernameService
   */
  protected $instagramFeedByUsernameService;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, InstagramFeedByUsernameService $instagram_Feed_By_UsernameService) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->InstagramFeedByUsernameService = $instagram_Feed_By_UsernameService;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {

    return new static(
          $plugin_id,
          $plugin_definition,
          $configuration['field_definition'],
          $configuration['settings'],
          $configuration['label'],
          $configuration['view_mode'],
          $configuration['third_party_settings'],
          $container->get('instagram_feed_by_username_service'));
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
          // Implement default settings.
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t("Displays the random string.");
    return $summary;
  }

  /**
   * Builds a renderable array for a field value.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The field values to be rendered.
   * @param string $langcode
   *   The language that should be used to render the field.
   *
   * @return array
   *   A renderable array for $items, as an array of child elements keyed by
   *   consecutive numeric indexes starting from 0.
   */

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $elements["#attached"]["library"][] =
            "instagram_feed_by_username/instagram_feed_by_username";
    $elements["#attributes"]["enctype"] = "multipart/form-data";
    foreach ($items as $delta => $item) {
      // Render output using #markup from instagram_feed_by_username_service.
      $userName = $item->instagram_field;
      $userInstaFeeds = $this->InstagramFeedByUsernameService->posts($userName);
      $elements[$delta][] = [
        "#markup" => $userInstaFeeds,
      ];
    }
    return $elements;
  }

}
