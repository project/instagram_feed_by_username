<?php

namespace Drupal\instagram_feed_by_username\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'InstagramFeedByUsernameFieldWidget' widget.
 *
 * @FieldWidget(
 *   id = "InstagramFeedByUsernameFieldWidget",
 *   label = @Translation("Instagram username field widget"),
 *   field_types = {
 *     "instagram_field"
 *   }
 * )
 */
class InstagramFeedByUsernameFieldWidget extends WidgetBase {
  /**
   * Define the form for the field type.
   *
   * Inside this method we can define the form used to edit the field type.
   */

  /**
   * {@inheritdoc}
   */
  public function formElement(
        FieldItemListInterface $items,
        $delta,
        array $element,
        array &$form,
        FormStateInterface $form_state
    ) {
    // Instagram_field.
    $element["instagram_field"] = [
      "#type" => "textfield",
      "#title" => "Instagram username field",
      "#description" =>
      $this->t("Instagram username field to be used for alpha-numeric values"),
          // Set here the current value for this field,
          // or a defaule value (or null) if there is no a value.
      "#default_value" => $items[$delta]->instagram_field ?? NULL,
      "#weight" => 0,
    ];
    return $element;
  }

}
