<?php

namespace Drupal\instagram_feed_by_username\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Plugin implementation of the 'instagram_field' field type.
 *
 * @FieldType(
 *   id = "instagram_field",
 *   label = @Translation("Instagram username"),
 *   description = @Translation("This field is used to store alpha-numeric values."),
 *   default_widget = "InstagramFeedByUsernameFieldWidget",
 *   default_formatter = "InstagramFieldByUsernameFieldFormatter"
 * )
 */
class InstagramFeedByUsernameField extends FieldItemBase {
  /**
   * Field type properties definition.
   *
   * Inside this method we defines all the fields (properties) that our
   * custom field type will have.
   */

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(
        FieldStorageDefinitionInterface $definition
    ) {
    // Prevent early t() calls by using the TranslatableMarkup.
    $properties["instagram_field"] = DataDefinition::create(
          "string"
      )->setLabel(new TranslatableMarkup("Text value"));
    return $properties;
  }

  /**
   * Field type schema definition.
   *
   * Inside this method we defines the database schema used to store data for
   * our field type.
   */

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $definition) {
    $schema = [
      "columns" => [
        "instagram_field" => [
          "type" => "varchar",
          "length" => 255,
        ],
      ],
    ];
    return $schema;
  }

  /**
   * Define when the field type is empty.
   *
   * This method is important and used internally by Drupal. Take a moment
   * to define when the field fype must be considered empty.
   */

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->getValue();
    if (
          isset($value["instagram_field"]) &&
          $value["instagram_field"] != ""
      ) {
      return FALSE;
    }
    return TRUE;
  }

}
