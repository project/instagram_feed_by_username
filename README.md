Instagram Feed By Username

The Instagram Feeds By Username allows user to add a field 
in content type to display Instagram feed Content by username.
This module leverages a third-party 
<a href="https://api.woxo.tech/instagram?source=instagram ">
WOXO TECH</a> for this integration. 

For a full description of the module, visit the
project page.
Submit bug reports and feature suggestions, or track changes in the
issue queue.

Table of contents (TOC)
---------------------

 * Requirements
 * Installation
 * Configuration
 * Maintainers

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 *Install as you would normally install a contributed Drupal module. Visit
  [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules) for further information.


CONFIGURATION
-------------

To use:
    1. Before starting with this module make sure Instagram user 
    has their account public enable.
    2. Navigate to Administration > Extend > And Search 
    for Instagram Feed By Username Module
    3. Enable the Instagram Feed By Username Module 
    4. User can add instagram username field from 
    field type for any content type or block.
    5. While creating node user can insert any public 
    instagram profile's username in the field. 
    6. Now we can see the recent posts of user's Instagram feed.

Maintainers
-----------

- Pankaj kumar  - (https://www.drupal.org/u/pankaj_lnweb)
- Shikha Dawar  - (https://www.drupal.org/u/shikha_lnweb)
- Vivek kumar  - (https://www.drupal.org/u/vivek_lnwebworks)
- Bhumika Madan  - (https://www.drupal.org/u/bhumika_lnwebworks)
- kishan kumar  - (https://www.drupal.org/u/kishanlnwebworks)
